const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const Models = require('../models');
const Constants     = require('../config/appConstants');
// Setup options for JWT Strategy
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = Constants.SERVER.JWT_SECRET_KEY//jwtSecretKey;
// Create JWT Strategy
// module.exports = passport => {
    passport.use('user', new JwtStrategy(jwtOptions, 
        async function(payload, done) {
            try {
                var criteria = {
                    _id: payload.data._id,
                    email: payload.data.email,
                    lastLogin:payload.data.lastLogin
                }
				Models.Users.findOne(criteria, function (err, result) {
                    if (err){
                        return done(null, err);
                    }else if (result) {
                        return done(null, result);
                    }else {
                        return done(null, false);
                    }
                });
            } catch(e) {
                console.log('not local');
                console.log(e);
                // return done(e, false);
            }
        }
    ));


module.exports = {
    initialize: function () {
        return passport.initialize();
    },
    authenticateUser: function (req, res, next) {
        return passport.authenticate("user", {
            session: false
        }, (err, user, info) => {
             if (err) {
                return res.status(403).json({
                    'success': false,
                    'message': err,
                    'code': 403,
                    'body': {}
                });
            }
            if (info && info.hasOwnProperty('name') && info.name == 'JsonWebTokenError') {
                return res.status(403).json({
                    'success': false,
                    'message': 'Invalid Token.',
                    'code': 403,
                    'body': {}
                });
            } else if (user == false) {
                return res.status(403).json({
                    'success': false,
                    'message': 'Authorization is required.',
                    'code': 403,
                    'body': {}
                });
            }            
            // Forward user information to the next middleware
            req.user = user; 
            req.headers.language = "en"
            next();
        })(req, res, next);
    }
};


