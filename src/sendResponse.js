const AppConstraints  = require('../config/appConstants');
const responseMessage = require('../config/response-messages');
//var util = require('util');



exports.invalidAccessTokenError = function (statusCode, language, message, res) {
    let lang = language ? language : 'en';
    var successData = {
        status: AppConstraints.STATUSCODE.UNAUTHORIZE,
        data: {},
        message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};

exports.somethingWentWrongError = function (statusCode, language, message, res) {

    let lang = language ? language : 'en';
    var successData = {
        status: AppConstraints.STATUSCODE.INTERNAL_SERVER_ERROR,
        data: {},
        message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};


exports.accountBlockedOrDeleted = function (statusCode, language, message, res) {

    let lang = language ? language : 'en';
    var successData = {
        status: (statusCode === 402 ? AppConstraints.STATUSCODE.APP_ERROR : AppConstraints.STATUSCODE.ROLE_CHANGE),
        data: {},
        message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};



exports.sendSuccessData = async function (data, statusCode, language, message, res, decryptColumns = []) {

    let lang = language ? language : 'en';

    var successData = {
        status: 1,
        statusCode: statusCode,
        data: data,
        message:  message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};
exports.sendErrorMessage = function (statusCode, language, message, res) {

    let lang = language ? language : 'en';
    var successData = {
        status: 0,
        statusCode: statusCode,
        data: {},
        message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};

exports.sendErrorMessageData = async function (statusCode, language, message, data, res) {

    let lang = language ? language : 'en';
    var successData = {
        status: 0,
        statusCode: statusCode,
        data: data,
        message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};


exports.sendError = function (statusCode, error, language, res) {

    let lang = language ? language : 'en';
    var errorMessage = {
        statusCode:statusCode,
        data: {},
        message: error
    };
    sendData(errorMessage, statusCode, res);
};


exports.successStatusMsg = function (res) {

    var successMsg = {"status": "true"};
    sendData(successMsg, res);
};


async function sendData(data, statusCode, res) {
    return res.status(statusCode).json(data);
}


exports.sendData = async function (data, statusCode, res) {
    return res.status(statusCode).json(data);
};