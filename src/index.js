/**
 * Makes all features available to outer modules.
 */

let routes = []

 if(process.env.USER_ROUTES === 'True'){
     routes = [...routes, ...require('./app_apis/').routes]
 }


module.exports = {
    routes: routes,
};
