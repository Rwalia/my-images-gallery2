'use strict';

// npm modules
const express = require('express');
// router
const router = express.Router();
const requireAuthentication = require('../../../passport').authenticateUser;

// local modules
const controller = require('./controller');

//auth 
router.post('/register', controller.register);
router.post('/login', controller.login);
router.put('/logout', requireAuthentication, controller.logout);
router.get('/get_profile', requireAuthentication, controller.get_profile);
router.post('/resetpass',controller.resetpass);
router.post('/reset_password',controller.resetPassword);
router.get("/forgot_url/:hash", controller.forgotUrl);
router.get("/getImage/:id", controller.getImage);
router.post("/uploadImage", requireAuthentication, controller.uploadImage);



module.exports = router;
