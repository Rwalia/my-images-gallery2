'use strict';

const { Validator } = require('node-input-validator');
var jwt = require('jsonwebtoken');
const moment = require("moment");
const helper = require('../../../helper/helper');
const bcrypt = require('bcryptjs');
const Constants = require('../../../config/appConstants');
const Models = require('../../../models');
	

module.exports = {
    uploadImage: async function (req, res, next) {
        try {
            const required = {};
            const non_required = {};
            const requestdata = await helper.vaildObject(required, non_required, res);
            const alreadyexitImgByuser = await Models.Gallery.findOne({ userId:req.user._id});
            if(alreadyexitImgByuser) {
                return helper.failed(res, "This user already uploaded Image.")
            }
            
            const filevalidation = await helper.filevalidation(req.files, res);
            const alreadyexitImg = await Models.Gallery.findOne({ size:req.files.size,realname:req.files.name});
            if(alreadyexitImgByuser) {
                return helper.failed(res, "This Image already exist.")
            }           
            let PAYLOAD = req.body;
            let FILE_TYPE = 'image'; // image
            let FOLDER = 'user'; //PAYLOAD.folder;// user,category,events,etc
            let image_url = await helper.fileUploadMultiparty(req.files.image, FOLDER, FILE_TYPE);
            image_url.userId =req.user._id;
            let create_Gallery = await Models.Gallery.create(image_url);
            return helper.success(res, "Successufully uploaded",create_Gallery); 
            
        } catch (err) {
            console.log(err)
            return helper.failed(res, err)
        }
    },
    getImage: async function (req, res, next) {
        try {
            const required = {};
            const non_required = {};
            const requestdata = await helper.vaildObject(required, non_required, res);
            const getimage = await Models.Gallery.findOne({ _id:req.params.id});
            if(getimage) {
                let response={
                    original:req.protocol+"://"+req.get('host')+getimage.original,
                    thumbnail:req.protocol+"://"+req.get('host')+getimage.thumbnail
                }
                return helper.success(res, "Fetched Successufully.",response); 
            } else {
                return helper.failed(res, "Image not exist.")
            }            
        } catch (err) {
            console.log(err)
            return helper.failed(res, err)
        }
    },
    register: async (req, res) => {
		try {
			const required = {
                securitykey: req.headers.securitykey,
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
            };
            const nonRequired = {};
            let requestData = await helper.vaildObject(required, nonRequired, req);
			const {username, email, password} = requestData;
			//check exist user by email	
			const find_exist_user_by_email = await Models.Users.findOne({ email});			
			if(find_exist_user_by_email) {
				return helper.failed(res, "This email already exists.")
            }  
            //check exist user by email	
            const find_exist_user_by_username = await Models.Users.findOne({ username});			
			if(find_exist_user_by_username) {
				return helper.failed(res, "This Username already exists.")
            }           
			//hash the password
			const salt = bcrypt.genSaltSync(8);
			const passwordHash = bcrypt.hashSync(password, salt);
            requestData.password = passwordHash;
            requestData.lastLogin = helper.currenttime();
			let create_user = await Models.Users.create(requestData);
            if(create_user) {	
                let token = jwt.sign({
                    data: {
                        _id: create_user._id,
                        email: create_user.email,
                        lastLogin: create_user.lastLogin,
                    }
				}, Constants.SERVER.JWT_SECRET_KEY,{ expiresIn: Constants.SERVER.TOKEN_EXPIRATION });
                create_user=create_user.toJSON()
				create_user.accessToken = token;
				return helper.success(res, "Signup successfully.", create_user);
			} else {
				return helper.failed(res, "Error. Please try again.")
			}
  
        } catch (error) {
			return helper.failed(res, error)
		}
	},
	login: async (req, res) => {
		try {
			const required = {
                securitykey: req.headers.securitykey,
                email: req.body.email,
                password: req.body.password,
            };
            const nonRequired = {};
            let requestData = await helper.vaildObject(required, nonRequired, req);
			const { email, password} = requestData;
            let check_email_exists = await Models.Users.findOne({ email});
            //check Email
			if (!check_email_exists) {
				return helper.failed(res, "Invalid Credentials.")
			}
			//check password
			const passwordCorrect = await bcrypt.compare(password, check_email_exists.password);
            if(!passwordCorrect) {
				return helper.failed(res, "Invalid Credentials.")
			}
			let time = helper.currenttime();
			let login_user = await Models.Users.findOne(
				{"_id": check_email_exists._id },
			);
			login_user.lastLogin = time
			login_user.save()
				
			if(login_user){
				let token = jwt.sign({
                    data: {
                        _id: check_email_exists._id,
						email: check_email_exists.email,
						lastLogin: login_user.lastLogin,
                    }
                }, Constants.SERVER.JWT_SECRET_KEY, { expiresIn: Constants.SERVER.TOKEN_EXPIRATION });
                login_user=login_user.toJSON()
				login_user.accessToken = token;
			return helper.success(res, "Login successfully.", login_user);
			} else {
				return helper.failed(res, "Error. Please try again.")
			}
			
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
	},
    resetpass: async (req, res) => {
		try {
			const required = {
                securitykey: req.headers.securitykey,
                email: req.body.email,
            };
            const nonRequired = {};
            let requestData = await helper.vaildObject(required, nonRequired, req);
			const { email} = requestData;
            let check_email_exists = await Models.Users.findOne({ email});
            //check Email
			if (!check_email_exists) {
				return helper.failed(res, "Email not exist.")
			} 
            let forgotPasswordHash = helper.createSHA1()+"@"+ helper.currenttime();
            check_email_exists.forgot_password = forgotPasswordHash 
           const user = check_email_exists.save()
            let obj={
                "link":req.protocol+"://"+req.get('host')+"/user/forgot_url/"+check_email_exists.forgot_password
            }
			return helper.success(res, 'Forgot Password link.', obj);
		} catch (err) {
			return helper.error(res, err);
		}
	},
    forgotUrl: async (req, res) => {
		try {
           let forgot_password = req.params.hash
           let check_email_exists = await Models.Users.findOne({ forgot_password});
                if (check_email_exists) {
                    let temarray=forgot_password.split('@');
                    let timediff =await helper.currenttime() -temarray[1]
                    if (Constants.SERVER.LINK_EXPIRATION > timediff) {
                        res.render("reset_password", {
                            response: check_email_exists,
                            hash: req.params.hash
                        });
                    } else {
                        const html = `
                        <br/>
                        <br/>
                        <br/>
                        <div style="font-size: 50px;" >
                            <b><center>Link has been expired!</center><p>
                        </div>
                    `;
                        res.status(403).send(html);
                    }
                    
                } else {
                    const html = `
                <br/>
                <br/>
                <br/>
                <div style="font-size: 50px;" >
                    <b><center>Link has been expired!</center><p>
                </div>
            `;
                res.status(403).send(html);
            }			
		} catch (err) {
			return helper.failed(res, error)
		}
	},
    resetPassword: async (req, res) => {
		try {
			const { password, forgot_password } = { ...req.body };

			let check_email_exists = await Models.Users.findOne({ forgot_password});
			if (!check_email_exists) throw "Something went wrong.";
            const salt = bcrypt.genSaltSync(8);
			const passwordHash = bcrypt.hashSync(password, salt);
            check_email_exists.password = passwordHash;
			check_email_exists.forgot_password = '';
			const user = check_email_exists.save()
			if (user) {
				return helper.success(res, 'Password updated successfully.', {});
			} else {
				throw "Invalid User.";
			}

		} catch (err) {
			return helper.error(res, err);			
		}
	},
	logout: async (req, res) => {
		try {
			let CheckAuth = await Models.Users.findOne({_id: req.user._id});
			if (CheckAuth) {
                let Updateauth = await Models.Users.updateMany(
					{_id: req.user._id},
                   	{
                    	lastLogin: 0,
					}
				);
                if (Updateauth) {
                    let msg = "User Logged Out Successfully !"
                    return helper.success(res, msg, {});
                } else {
                    return helper.failed(res, "Something went wrong")
                }
            } else {
                let msg = "Invalid Token!";
                return helper.failed(res, msg)
            }
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
    },    
    forgot_password: async (req, res) => {
		try {
            let v = new Validator( req.body, {
                email: 'required|email', 
                role: 'required', 
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
               return helper.failed(res, errorsResponse)
            }
            const {email,role} = req.body;
            
            let check_email_exists = await Models.Users.findOne({ email,role });
            if (!check_email_exists) {
				return helper.failed(res, "Email does not exists.")
            }
            //let otp = '1111';
            let otp = Math.floor(1000 + Math.random() * 9000);
            // let to = requestdata.country_code + '' + requestdata.phone_number;
            let to = email;
            let html = await helper.resend_otp_html(otp)
            await helper.sendEmail({
                from: process.env.EMAIL_FROM,
                to: to,
                subject: 'Buskit Reset Otp',
                html: html
            });

            let update_otp = await Models.Users.findOne(
				{"_id": check_email_exists._id },
			);
			update_otp.otp = otp;
            update_otp.save();
            let msg = 'OTP sent to your email.';
            return helper.success(res, msg, update_otp);
            
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
    },
    get_profile: async (req, res) => {
		try {
            const required = {
                securitykey: req.headers.securitykey,
            };
            const nonRequired = {};
            let requestData = await helper.vaildObject(required, nonRequired, req);
            let _id = req.user._id;
			let get_detail = await Models.Users.findOne({ _id });
			if(get_detail){
				return helper.success(res, "Get profile.", get_detail);
			}
			else{
				return helper.failed(res, "invalid User")
			}
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
    },

};