/**
 * Makes all features available to outer modules.
 */

const express = require('express')
const router = express.Router();

router.use('/', require('./app_apis').Routes);

module.exports = {
    routes: [router],
};