
module.exports = {
    STATUS_MSG: {
        SUCCESS: {
            DEFAULT: {
                statusCode: 200,
                message: {
                    en: 'Success',
                },
                type: 'DEFAULT'
            },
            SUCCESS: {
                statusCode: 200,
                message: {
                    en: 'Success.',
                },
                type: 'SUCCESS'
            },
            CREATED: {
                statusCode: 200,
                message: {
                    en: 'Successfully created.',
                },
                type: 'CREATED'
            },
            DELETED: {
                statusCode: 200,
                message: {
                    en: 'Successfully deleted.',
                },
                type: 'DELETED'
            },
            LOGIN_SUCCESS: {
                statusCode: 200,
                message: {
                    en: 'Logged in successfully',
                },
                type: 'LOGIN_SUCCESS'
            },
            LOGIN_ERROR: {
                statusCode: 200,
                message: {
                    en: 'Email or password is incorrect',
                },
                type: 'LOGIN_ERROR'
            },            
            NOT_REGISTERED: {
                statusCode: 400,
                message: {
                    en: 'Currently, you are not registered with us.'
                },
                type: 'NOT_REGISTERED'
            },
            INCORRECT_OLD_PASSWORD: {
                message: {
                    en: 'Your old password is incorrect.',
                },
                statusCode: 400,
                type: 'INCORRECT_OLD_PASSWORD'
            },
            INCORRECT_CONFIRM_PASSWORD: {
                message: {
                    en: 'New password and confirm password do not match. Please try again.',
                },
                statusCode: 400,
                type: 'INCORRECT_OLD_PASSWORD'
            },

            INCORRECT_OLD_NEW_PASSWORD: {
                message: {
                    en: 'Old and New password both are same.Please change new password.',
                },
                statusCode: 400,
                type: 'INCORRECT_OLD_NEW_PASSWORD'
            },

            OBJECT: {
                OBJECT_DUPLICATE_NAME: {
                    statusCode: 400,
                    message: {
                        en: 'Object with this name already exists.'
                    },
                    type: 'OBJECT_DUPLICATE_NAME'
                },
                SUBSCRIPTION_EXPIRED: {
                    statusCode: 400,
                    message: {
                        en: 'Your subscription has been expired, Please renew your subscription.'
                    },
                    type: 'SUBSCRIPTION_EXPIRED'
                },
                LIMIT_EXCEEDING: {
                    statusCode: 400,
                    message: {
                        en: 'You have reached limit to add objects.'
                    },
                    type: 'LIMIT_EXCEEDING'
                },

            },
        }
    }
};
