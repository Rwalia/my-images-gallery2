let mongoose=require('mongoose');
let Schema=mongoose.Schema;

let GallerySchema = new Schema({
    original:           {type: String, default:''},
    thumbnail:          {type: String, default:''},
    realname:          {type: String, default:''},
    size:          {type: String, default:''},
    folder:          {type: String, default:''},
    file_type:          {type: String, default:''},
    userId:                 {type: Schema.Types.ObjectId,ref:'Users'},
},{timestamps:true});

module.exports = mongoose.model('gallery',GallerySchema);
