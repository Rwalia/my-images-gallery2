let mongoose = require('mongoose');
// let async = require('async'),
// randomstring = require("randomstring"),
// user_plugin = require("../plugins").userPlugin;         // import mongo plugin for pre save hook

let Schema = mongoose.Schema;


let Userschema = new Schema({ 
    username: { type: String, index: true, default: "" },
    email: { type: String, index: true, default: "" },
    password: { type: String, default: "" },
    forgot_password: { type: String, default: "" },
    lastLogin: { type: Date, default: null },
    language: { type: String, enum: ['en', 'ar'], default: 'en' },
    status: { type: String, enum: ['0', '1'], default: '1' }, 
    isActive: { type: String, default: '1' },
}, { timestamps: true });


const user = mongoose.model('Users', Userschema);
module.exports = user;
