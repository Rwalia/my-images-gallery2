require('./env-validation');
var createError = require('http-errors');
var express = require('express');
const cors = require('cors');
var app = express();
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const server = require('http').Server(app);
const passport = require('passport');
const mongoose = require('mongoose');
var Constants = require('./config/appConstants');
const multipart = require('connect-multiparty');



global.ObjectId = mongoose.Types.ObjectId;

var mongoURI = require('./config/dbConfig').mongo;

const [app_apis] = require('./src/app_apis').routes;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.set(Constants.SERVER.JWT_SECRET_KEY, Constants.SERVER.JWT_SECRET_VAL);

app.use(passport.initialize());
app.use(cors());
app.use(logger('dev'));
app.use(express.json({ limit: '200mb' }));   // support parsing of application/json type post data
app.use(express.urlencoded({ limit: '200mb', extended: true })); //support parsing of application/x-www-form-urlencoded post data
app.use(cookieParser());
app.use(multipart());

app.use('/node_modules_url', express.static(path.join(__dirname, 'node_modules')));

app.use(express.static(path.join(__dirname, 'public')));

mongoose.Promise = global.Promise;
console.log(mongoURI,"=================");
mongoose.connect(mongoURI, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true, useUnifiedTopology: true });
mongoose.connection.on('error', function (err) {
    console.log(err);
    console.log('error in connecting, process is exiting ...');
    process.exit();
});

mongoose.connection.once('open', function () {
    console.log('Successfully connected to database');
});

app.use('/user', app_apis);
app.use(function (err, req, res, next) {
    let status = 500, json = {
        status: 0,
        message: err.message.en || err.message
    };
    if (err.isBoom) {
        //json = err.output.payload;
        json = { status: 0, message: err.data[0].message }
        status = err.output.statusCode;
    }
    return res.status(status).json(json);
});

app.options('/*', cors()) // enable pre-flight request for DELETE request

// SocketManager.connectSocket(server,redisClient);
server.listen(process.env.PORT, function () {
    console.log('Node app is running on port', process.env.PORT);
});
module.exports = app;
