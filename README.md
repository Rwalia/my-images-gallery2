<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">My Images Gallery</h3>
  <br><br>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#technologies-used">Technologies Used</a></li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#api-documentation">API Documentation</a></li>
    <li><a href="#apis-development">APIs Development</a></li>
  </ol>
</details>



<!-- Technologies Used -->
## Technologies Used

* [NodeJS (v12.22.4) for Backend](https://nodejs.org/)
* [MongoDB(v3.6.3)](https://www.mongodb.com/)
<p align="right">(<a href="#top">back to top</a>)</p>


## apis

* [documentation](https://documenter.getpostman.com/view/7898668/UVkvJY3E)
* [Postman collection](https://www.getpostman.com/collections/53a4a60b86662b2305a4)
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

Instructions on setting up project locally.
To get a local copy up and running, follow these simple steps:-

### Prerequisites

* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone 
   ```
2. Install NPM packages
   ```sh
   npm install or yarn install # [install modules using npm or yarn]
   ```

<p align="right">(<a href="#top">back to top</a>)</p>





<p align="right">(<a href="#top">back to top</a>)</p>

## Environment File Structure
```sh
  NODE_ENV="local" # [local/dev/prod]
  PORT=7860
  ADMIN_ROUTES=True # [true to enable admin panel apis, false to disable]
  USER_ROUTES=True # [true to enable app apis, false to disable]
  COMMON_ROUTES=True # [true to enable common(for app and admin panel) apis, false to disable]
  MONGO_DB_USER= # [database username]
  MONGO_DB_PASSWORD= # [database password]
  MONGO_DB_NAME= # [database name]
  MONGO_DB_HOST= # [database hostname/IP]
  ```


<p align="right">(<a href="#top">back to top</a>)</p>

