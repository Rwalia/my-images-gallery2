var path = require('path');

const constants = require('../config/appConstants');
const { Validator } = require('node-input-validator');
const fileExtension = require('file-extension');
const util = require('util');
const sharp = require('sharp') //for image thumbnail
const crypto = require('crypto');
const fs = require('fs-extra')
const Models = require('../models');
/*
|----------------------------------------------------------------------------------------------------------------------------
|   Exporting all methods
|----------------------------------------------------------------------------------------------------------------------------
*/
module.exports = {  
    createSHA1: function () {
        let key = 'abc' + new Date().getTime();
        return crypto.createHash('sha1').update(key).digest('hex');
    },
    readFile: async (path) => {
        console.log("  ************ readFile *******************")
        console.log(path, "  ************ pathreadFile *******************")
        return new Promise((resolve, reject) => {
            const readFile = util.promisify(fs.readFile);
            readFile(path).then((buffer) => {
                resolve(buffer);
            }).catch((error) => {
                reject(error);
            });
        });
    },
    writeFile: async (path, buffer) => {
        console.log("  ************ write file *******************")
        return new Promise((resolve, reject) => {
            const writeFile1 = util.promisify(fs.writeFile);
            writeFile1(path, buffer).then((buffer) => {
                resolve(buffer);
            }).catch((error) => {
                reject(error);
            });
        });
    },
    ValidatorFunc: async function (v,res) {
        try {
            let errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
                return res.status(400).json({
                    'success': false,
                    'code': 400,
                    'message': errorsResponse,
                    'body': {}
                });
             }
             return true
        } catch (err) {
            return res.status(400).json({
                'success': false,
                'code': 400,
                'message': err,
                'body': {}
            });
        }        
    },
    fileUploadMultiparty: async function (FILE, FOLDER, FILE_TYPE) {
        try {
            var FILENAME = FILE.name; // actual filename of file
            var size = FILE.size; // actual filename of file
            var FILEPATH = FILE.path; // will be put into a temp directory

            THUMBNAIL_IMAGE_SIZE = 300
            THUMBNAIL_IMAGE_QUALITY = 100

            let EXT = fileExtension(FILENAME); //get extension
            EXT = EXT ? EXT : 'jpg';
            FOLDER_PATH = FOLDER ? (FOLDER + "/") : ""; // if folder name then add following "/" 
            var ORIGINAL_FILE_UPLOAD_PATH = "/uploads/" + FOLDER_PATH;
            var THUMBNAIL_FILE_UPLOAD_PATH = "/uploads/" + FOLDER_PATH;
            var NEW_FILE_NAME = (new Date()).getTime() + "-" + "file." + EXT;
            var NEW_THUMBNAIL_NAME = (new Date()).getTime() + "-" + "thumbnail" + "-" + "file." + ((FILE_TYPE == "video") ? "jpg" : EXT);

            let NEWPATH = path.join(__dirname, '../public/', ORIGINAL_FILE_UPLOAD_PATH, NEW_FILE_NAME);
            let THUMBNAIL_PATH = path.join(__dirname, '../public/', ORIGINAL_FILE_UPLOAD_PATH, NEW_THUMBNAIL_NAME);

            let FILE_OBJECT = {
                "original": '',
                "thumbnail": '',
                "realname": FILENAME,
                "size": size,
                "folder": FOLDER,
                "file_type": FILE_TYPE
            }
            console.log(FILEPATH,"***********************************")
            let BUFFER = await this.readFile(FILEPATH); //read file from temp path
            await this.writeFile(NEWPATH, BUFFER); //write file to destination

            FILE_OBJECT.original = THUMBNAIL_FILE_UPLOAD_PATH + NEW_FILE_NAME;

            let THUMB_BUFFER = "";

            if (FILE_TYPE == 'image') { // image thumbnail code
                var THUMB_IMAGE_TYPE = (EXT == "png") ? "png" : "jpeg";
                THUMB_BUFFER = await sharp(BUFFER)
                    .resize(THUMBNAIL_IMAGE_SIZE)
                    .toFormat(THUMB_IMAGE_TYPE, {
                        quality: THUMBNAIL_IMAGE_QUALITY
                    })
                    .toBuffer();

                FILE_OBJECT.thumbnail = THUMBNAIL_FILE_UPLOAD_PATH + NEW_THUMBNAIL_NAME;
                await this.writeFile(THUMBNAIL_PATH, THUMB_BUFFER);
            } 
            else{
                FILE_OBJECT.thumbnail = ''
            }
            return FILE_OBJECT;
        } catch (err) {
            console.log(err);
            throw err;

        }
    },

    vaildObject: async function (required, non_required, res) {
        let message = '';
        let empty = [];

        for (let key in required) {
            if (required.hasOwnProperty(key)) {
                if (required[key] == undefined || required[key] === '' && (required[key] !== '0' || required[key] !== 0)) {
                    empty.push(key);
                }
            }
        }
        if (empty.length != 0) {
            message = empty.toString();
            if (empty.length > 1) {
                message += " fields are required"
            } else {
                message += " field is required"
            }
            throw {
                'code': 400,
                'message': message
            }
        } else {
            if (required.hasOwnProperty('securitykey')) {
                if (required.securitykey != process.env.SECURITY_KEY) {
                  message = "Invalid security key";
                  throw {
                    'code': 400,
                    'message': message
                  }
                }
              }

            const merge_object = Object.assign(required, non_required);
            // delete merge_object.securitykey;

            if (merge_object.hasOwnProperty('password') && merge_object.password == '') {
                delete merge_object.password;
            }

            for (let data in merge_object) {
                if (merge_object[data] == undefined) {
                    delete merge_object[data];
                } else {
                    if (typeof merge_object[data] == 'string') {
                        merge_object[data] = merge_object[data].trim();
                    }
                }
            }

            return merge_object;
        }
    },
    filevalidation: function (files, res) {
        if (files.image == undefined) {
            return res.status(400).json({
                'success': false,
                'message': "Please select image",
                'code': 400,
                'body': {}
            });
        }
        if (files && files.image && Array.isArray(files.image)) {
            return res.status(400).json({
                'success': false,
                'message': "upload only single image.",
                'code': 400,
                'body': {}
            });
        }
        if (files.image.name == "") { 
            return res.status(400).json({
                'success': false,
                'message': "Error - Image can't be empty",
                'code': 400,
                'body': {}
            });
        }
        if (!["image/jpeg","image/png"].includes(files.image.type)) {
            return res.status(400).json({
                'success': false,
                'message': "Only accept jpeg or png Image",
                'code': 400,
                'body': {}
            });
        }
        if (files.image.size  > 1048576 ) {
            return res.status(400).json({
                'success': false,
                'message': "Only accept less then 1MB Image",
                'code': 400,
                'body': {}
            });
        }
        return true
    },
    error: function (res, err, req) {
        let code = (typeof err === 'object') ? (err.code) ? err.code : 403 : 403;
        let message = (typeof err === 'object') ? (err.message ? err.message : '') : err;
      
        if (req) {
            req.flash('flashMessage', {
                color: 'error',
                message
            });

            const originalUrl = req.originalUrl.split('/')[1];
            return res.redirect(`/${originalUrl}`);
        }

        return res.status(code).json({
            'success': false,
            'message': message,
            'code': code,
            'body': {}
        });

    },
    success: function (res, message = '', body = {}) {
        return res.status(200).json({
            'success': true,
            'code': 200,
            'message': message,
            'body': body
        });
    },

    bcryptHash: (myPlaintextPassword, saltRounds = 10) => {
        const bcrypt = require('bcrypt');
        const salt = bcrypt.genSaltSync(saltRounds);
        let hash = bcrypt.hashSync(myPlaintextPassword, salt);
        hash = hash.replace('$2b$', '$2y$');
        return hash;
    },

    failed: function (res, message = '') {
        message = (typeof message === 'object') ? (message.message ? message.message : '') : message;
        return res.status(400).json({
            'success': false,
            'code': 400,
            'message': message,
            'body': {}
        });
    },
    unixTimestamp: function () {
        var time = Date.now();
        var n = time / 1000;
        return time = Math.floor(n);
    },
    currenttime: function () {
        var time = Date.now();
        return time;
    },
    create_auth() {
        try {
            let current_date = (new Date()).valueOf().toString();
            let random = Math.random().toString();
            return crypto.createHash('sha1').update(current_date + random).digest('hex');
        } catch (err) {
            throw err;
        }
    },
    
}